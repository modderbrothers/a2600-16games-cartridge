![modderbrothers](./assets/product.png)

# Atari 2600 16 Games Cartridge

[Version Française Ici](./LISEZMOI.md)

## Description
Play up to 16 games in a single cartridge on your Atari 2600!

Features:
* Up to **16** games on a single cartridge (32KB maximum per game)
* Reprogrammable EPROM (you can also replace it with other compatible EPROM/EEPROM)

## Manual
The manual is available at [./manual/](./manual/)

## Technical Documents
Technicals documents are available at [./technical-docs/](./technical-docs/)

## Production files
Production files are available at [./production-files/](./production-files/)

## Disclaimers

This original ModderBrothers mod is distributed under the **[CERN Open Hardware 2.0 - Weakly Reciprocal](./LICENSE.md)** (CERN-OHL-W 2.0) license and is subject to upgrade to higher versions of this same license.

Regarding warranties and liability, here is the applicable license extract:

##### 6.1 DISCLAIMER OF WARRANTY
The Covered Source and any Products
are provided 'as is' and any express or implied warranties,
including, but not limited to, implied warranties of
merchantability, of satisfactory quality, non-infringement of
third party rights, and fitness for a particular purpose or use
are disclaimed in respect of any Source or Product to the
maximum extent permitted by law. The Licensor makes no
representation that any Source or Product does not or will not
infringe any patent, copyright, trade secret or other
proprietary right. The entire risk as to the use, quality, and
performance of any Source or Product shall be with You and not
the Licensor. This disclaimer of warranty is an essential part
of this Licence and a condition for the grant of any rights
granted under this Licence.

##### 6.2 EXCLUSION AND LIMITATION OF LIABILITY
The Licensor shall, to
the maximum extent permitted by law, have no liability for
direct, indirect, special, incidental, consequential, exemplary,
punitive or other damages of any character including, without
limitation, procurement of substitute goods or services, loss of
use, data or profits, or business interruption, however caused
and on any theory of contract, warranty, tort (including
negligence), product liability or otherwise, arising in any way
in relation to the Covered Source, modified Covered Source
and/or the Making or Conveyance of a Product, even if advised of
the possibility of such damages, and You shall hold the
Licensor(s) free and harmless from any liability, costs,
damages, fees and expenses, including claims by third parties,
in relation to such use.